import pikepdf
import sys
import os


def remove_restrictions(input_path, output_path):
    try:
        with pikepdf.open(input_path) as pdf:
            # Set permissions - allowing print, copy, and modify
            pdf.permissions = pikepdf.Permissions(extract=True)

            # Save the new PDF
            pdf.save(output_path)
            print(f"Restrictions removed. New file saved as '{output_path}'")
    except pikepdf.PasswordError:
        print(f"A password is required to open '{input_path}'. Skipping.")
    except Exception as e:
        print(f"An error occurred with '{input_path}': {e}")


def process_directory(directory):
    for filename in os.listdir(directory):
        if filename.lower().endswith('.pdf'):
            input_path = os.path.join(directory, filename)
            output_path = os.path.join(directory, filename[:-4] + "_removed.pdf")
            remove_restrictions(input_path, output_path)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python removeReadOnly.py <directory_path>")
        sys.exit(1)

    directory_path = sys.argv[1]
    process_directory(directory_path)

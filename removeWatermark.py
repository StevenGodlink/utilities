import sys
import os
import pikepdf


def inspect_pdf(input_pdf, output_pdf, param):
    with pikepdf.open(input_pdf) as pdf:
        for i, page in enumerate(pdf.pages):
            instructions = pikepdf.parse_content_stream(page)
            modified_instructions = instructions[:param]
            new_stream = pikepdf.unparse_content_stream(modified_instructions)
            page.Contents = pikepdf.Stream(pdf, new_stream)
        pdf.save(output_pdf)


def process_directory(input_dir, output_dir, param):
    # Create the output directory if it does not exist
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Process each PDF in the input directory
    for filename in os.listdir(input_dir):
        if filename.endswith(".pdf"):
            input_pdf = os.path.join(input_dir, filename)
            output_pdf = os.path.join(output_dir, filename)
            inspect_pdf(input_pdf, output_pdf, param)


if __name__ == "__main__":
    try:
        if len(sys.argv) < 4:
            raise Exception("Usage: python3 script.py <input_directory> <output_directory> <param>")
        input_dir = sys.argv[1]
        output_dir = sys.argv[2]
        param = int(sys.argv[3])

        process_directory(input_dir, output_dir, param)
    except Exception as e:
        print(e)
